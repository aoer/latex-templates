# Andy's $\LaTeX$ Templates

This repository contains the $\LaTeX$ templates that I use for different kinds of documents. The folders in the directory are named by the type of document that they are for.

### How to use it?

Just copy the required folder and modify the metadata in the main file (marked with `TODO:` comment). The main file is named the same as the folder. Then just add the content to the document as you want.

## [Poster](poster)
This template is based on the [`tikzposter` class](https://texdoc.org/serve/tikzposter.pdf/0). The template uses the `Simple` theme and is modified a bit from the original theme and includes a QR code in the title matter.

## [Slides](slides)
This is a $\LaTeX$ [beamer](http://www.beamer.plus/home.html) template. The template is based on the `Dresden` theme and has been modified to contain less metadata on the slide.

## Examples
Here are some examples for documents generated the templates in this directory.

- [Conference slides](https://aoertel.de/pdf/presentations/presentation_core-guided-MaxSAT_CADE-29.pdf)

## Licence
The templates are licensed under the Apache License 2.0. See [LICENCE](LICENCE).